
SEE NOTES REPO FOR FULL DOCUMENTATION WITH IMAGES

## Problems Solved

##### 1) TCP Sockets Not Having concept of "Message"
- TCP sockets do not have a concept of what a message is, they only deal with stream of bytes
- TCP guarantees data is transmitted from source to destination in order
- We need to define a protocol at the application level and define where a message ends and where a message begins
- I used **Fixed Length Header**,  a header of fixed length containing the length of the following bytes which represent a message


Ex: '5      hello'  (there should be 4 spaces after 5, for a total of 5 characters)
- header length is a parameter we need to define, depending on how big of a message we want to be able to receive
- above, the header length = 5, meaning we can receive messages that are 99999 bytes long (and 99999 characters for most used characters)

source: https://stackoverflow.com/questions/2862071/how-large-should-my-recv-buffer-be-when-calling-recv-in-the-socket-library

![](assets/markdown-img-paste-20201125222933532.png)


although The below message leads me to believe I implemented it incorrectly and I can not expect for header to be read in one receive call and message in the other

https://stackoverflow.com/questions/41382127/how-does-the-python-socket-recv-method-know-that-the-end-of-the-message-has-be


![](images_for_md_file/Screenshot from 2020-11-28 10-17-23.png)

<br>

##### 2) In client script, the Client needs to be able to Receive messages without being block by `input()`, which is to write messages
- Solution is to define a new function just for receiving messages in a while loop, **and create a new thread to execute that function without block the main thread which will be waiting for user to write message**

<br>

##### 3) What is in the tuple `ip_port` returned from `client_socket, ip_port = server.accept()`
- `ip_port[0]` is the ip address of the client
- `ip_port[1]` is the port number of the client


![](assets/markdown-img-paste-20201125214735481.png)

- After client starts 3 way handshake with server, which occurs when `accept()` is called, a new socket is generated. The client will use it to communicate with the server
- The server will send the data to the client's IP and port, 45292 in this case
- we can see below that in the server script, the socket has two fields `ladr` and `radr`, local and remote address, in other words, a source address to send packets from and a destination address to send packets to.
  - **on the client script, for the same client, these addresses will be flipped because destination address is now the server's**

![](assets/markdown-img-paste-20201125224518878.png)

<br>

Same Explanation on SO:
![](assets/markdown-img-paste-20201125214000481.png)


## Intro
**Socket Programming:**
- refers to writing programs that execute across multiple computers in which the devices are all connected to each other using a network


**There are two communication protocols that one can use for socket programming**

1) User Datagram protocol (UDP)
2) Transfer Control Protocol (TCP)

<br>

## Important Details about TCP Sockets
1) TCP has no concept of messages, the concept has to be implemented in application layer
2) Guarantees the bytes will arrive in the same order
3) Does Not guarantee the bytes will arrive chopped up in the same way, only order is guaranteed

## How TCP Sockets Work
**Socket:** one endpoint of a two-way communication link between two programs running on different computers on a network

**Server Socket Will Have following Responsibility:**

1) waiting for clients to connect
2) Once connection is established with the client socket, **Server Socket will wait for messages**

Additionally, most APIs, and languages have an `accept()` method, which should only be called after the serve socket has been binded, and is listening to a port. This method will block socket until a client socket establishes a connection, then the method executes returning a socket that can be used to communicate with client.

<br>

### Binding and listening to a port
- Server socket needs to be:
1) binded to a port
2) listening to that port

This is needed because otherwise messages sent by clients will not know where exactly within IP address the server socket is waiting for the data

- **IP address is analogous to zip code and street address**
- **Port number = house number**

<br>

**More technical Explanation:**

- A **Socket** is bound to a port number so that the transport layer can identify the application (process) that data is destined to be sent to

![](assets/markdown-img-paste-20201116094143452.png)

<br>

**General Steps Needed to Successfully communicate Between client and server sockets:**

![](assets/markdown-img-paste-20201116083717244.png)




<br>

**Note:** Some APIs may do multiple steps at once, for example

<br>

**Creating a Server Socket Using `ServerSocket` in Java**


```java
//Creating a new Server Socket will also bind it to a port, and start listening
         server = new ServerSocket(5000);
         System.out.println("Server started");

         System.out.println("Waiting for a client ...");


         //accept blocks until client establishes connection
         socket = server.accept();
         System.out.println("Client accepted");

```
- Binding and listening happens inside the constructor
- Note that an ip address is not passed to constructor, I would assume the ip address is found via another way inside the constructor code


Before `ServerSocket` constructor executes:

```bash
(base) petrarca@petrarca:~ $ netstat -nepal|grep 5000


```

After `ServerSocket` constructor executes:

```java
(base) petrarca@petrarca:~ $ netstat -nepal|grep 5000
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
tcp6       0      0 :::5000                 :::*                    LISTEN      1000       3125198    6033/java           
```

We can see that our server socket is now listening to port 5000


<br>

**Creating Server Socket by using `socket` in Python**

```python
import socket

s =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((socket.getHostname(), 1234))
s.listen(5)

while True:
    clientSocket, address = s.accept()
    print(f"Connection from {adress} has been established!")
    clientsocket.send(bytes("Welcome to the server!,"utf-8))
```


## Creating a Echo Server
- client script waits for input into console
- The message is then sent to server, which receives message, and sends it back to client, where it is displayed in console


`Server.py`
```python
import socket


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serverSocket:
    serverSocket.bind((socket.gethostname(), 1234))
    serverSocket.listen(5)

##blocks until client socket connects
# When client socket connects, method unblocks and returns a socket object, used to communicate with client socket
    connectionSocket, address = serverSocket.accept()
    with connectionSocket:
        print("server connected to ",address)
        while True:

            #source: https://docs.python.org/2/howto/sockets.html ,using sockets portion
            #recv(bufferSize) blocks, and returns when the associated network buffer has been filled (send) or empties (recv). Then they tell you how many bytes they handles, it is your repsonsibility to call them again until your message has been completely dealt with
            #when receives returns 0 bytes, it means the other side has been closed. You will not receive any more data on this connection.
            msg = connectionSocket.recv(4)
            if not msg:
                break
            connectionSocket.sendall(msg)




###with is used to automatically close socket

#https://www.geeksforgeeks.org/with-statement-in-python/

# serverSocket =  socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# try:
#     serverSocket.bind((socket.gethostname(), 1234))
#     serverSocket.listen(5)
#     connectionSocket, address = serverSocket.accept()
#     try:
#         print("server connected to ",address)
#         while True:
#             #receives blocks, and returns when the associated network buffer has been filled (send) or empties (recv). Then they tell you how many bytes they handles, it is your repsonsibility to call them again until your message has been completely dealt with
#             #when receives returns 0 bytes, it means the other side has been closed. You will not receive any more data on this connection.
#             msg = connectionSocket.recv(1024)
#             if not msg:
#                 break
#             connectionSocket.sendall(msg)
#     finally:
#         connectionSocket.close()
# finally:
#     serverSocket.close()

```
`client.py`
```python
import socket

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as clientSocket:

    ##server socket has to be binded and listening otherwise error occurs
    clientSocket.connect((socket.gethostname(), 1237))

    while True:

        input1Msg = input()

        clientSocket.send(bytes(input1Msg,"utf-8"))

        print(clientSocket.recv(1024).decode("utf-8"))

```


**Note:**
- `serverSocket.accept()` blocks until `clientSocket.connect()` where `clientSocket` was constructed with the address and port number that the `serverSocket` is listening to
- `socket.recv(BUFFER_SIZE)` blocks and returns as soon as the method `connectionSocket.send(...)` is called of the associated socket

<br>

### `recv(BUFFER_SIZE)`
- It is a blocking method that reads in bytes from buffer (temporary storage area for bytes received from the other socket) up to `BUFFER_SIZE`, but it does not necessarily read exactly `BUFFER_SIZE`

<br>

#### When does it return?

- blocks until, `send(..)` or `sendall(..)` is used by the other connected socket, then it returns

<br>

#### What does it return?
*If the other connected socket has not been closed:*
- returns a bytes, therefore, decoding into a string is necessary

*If the other socket has been closed:*
- returns an empty byte string



![](assets/markdown-img-paste-20201122103254854.png)


More details:

source: https://stackoverflow.com/questions/41382127/how-does-the-python-socket-recv-method-know-that-the-end-of-the-message-has-be

![](assets/markdown-img-paste-20201122104342569.png)


**Important detail**
- When you call `recv(n)` that is only guaranteed to return at most n bytes, not exactly n bytes.

source: https://stackoverflow.com/questions/56813704/socket-programming-python-how-to-make-sure-entire-message-is-received

![](assets/markdown-img-paste-20201122104613231.png)

