import socket
import threading

from Client import Client

"""
Stories:
- fix application layer protocol, right now code breaks if recv(..) decides to return less bytes than the argument I am using
- handle client closing terminal
- allow user to enter name they want to appear next to their prompt
- give welcome message to user and disconnect message when user disconnects or closes terminal  (done)
- is socket a shared resource and if so, should I synchronize?
- how can I broadcast message to all clients concurrently instead of looping through list of clients which would be very slow if we have a lot of chat users
- Figure out exception handling scenarios
- make it into a web application
"""

"""
When client socket connects to the server using accept() (via IP and port), the communication between that client and server is handled in a separate thread.

In that thread, client again blocks with recv(n) in while True loop waiting for messages which will be coming in from clients.

When a message from a client arrives

"""

HEADER_LENGTH = 64
PORT = 7023

# socket.gethostname() returns the hostname, a name representing this computer, then when pass it to gethostbyname() to get IP address
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "{quit}"
clients = []

# create a TCP socket


"""
function will bind server socket and start listening on port, and handle new connections and distribute those to threads
"""


def start_server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.bind(ADDR)
        server.listen(5)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        print(f'server is listening on IP: {ADDR[0]}, port: {ADDR[1]}')
        while True:
            client_socket, ip_port = server.accept()
            """with client_socket:     if i were to use with to manage socket connection , would I do it in main thread or in thread handling individual connection"""
            # note, i tried using with here, but connection seems oo be closed too soon
            welcome_message = add_header_to_message("Welcome to the chat server! Now enter your name to connect")
            client_socket.sendall(bytes(welcome_message.encode(FORMAT)))
            # clients.append(client_socket)
            client = Client(ip_port, client_socket)
            clients.append(client)
            threading.Thread(target=handle_conn, args=(client,)).start()
            print(f"ACTIVE CONNECTIONS: {len(clients)}")


"""
used to handle the individual connection and interaction between a single client and the server
"""


def handle_conn(client):
    print(f"Connection with client established at address: {client.ip}:{client.client_port}")
    name = wait_for_client_response(client)
    client.display_name = name
    client.socket.sendall(
        # when we use bytes function on a string, we need to specify what encoding we want to use to encode the string
        bytes(add_header_to_message(
            f"Welcome to the chat server {client.display_name}! Great to have you, you can begin chatting :) When you are finished chatting you can enter {DISCONNECT_MESSAGE} to exit the chat").encode(
            FORMAT))
    )
    broadcast_to_all_clients(f"{client.display_name} has joined the chat", client)
    connected = True
    while connected:
        message = wait_for_client_response(client)
        """We also need to come up with protocol to handle their disconnection cleanly,  right now we will implement this by closing the connection when we remove the disconnect message from the client"""
        if message == DISCONNECT_MESSAGE:
            clients.remove(client)
            message = f"{client.display_name} has left the chat"
            connected = False
            client.socket.close()


        """iterable is expected, therefore, we can not pass the message variable which is a string to each character will be iterated over. We instead provide string as tuple and include comma"""
        # thread = threading.Thread(target=broadcast_to_all_clients, args=(message,))
        # thread.start()

        broadcast_to_all_clients(message, client)


def add_header_to_message(msg):
    # finds length of message to be sent, then addings space padding to the numeric value and appends actual message to the end
    return f'{len(msg):<{HEADER_LENGTH}}' + msg


def broadcast_to_all_clients(msg, client_that_messaged):
    for client in clients:
        if client != client_that_messaged:
            msg_with_header = add_header_to_message(msg)
            # why do we need to encode, isnt bytes() already turning string into bytes
            client.socket.sendall(bytes(msg_with_header.encode(FORMAT)))


def wait_for_client_response(client):
    message_length = determine_message_length(client)
    return client.socket.recv(message_length).decode(FORMAT)


def determine_message_length(client):
    header = client.socket.recv(HEADER_LENGTH).decode(FORMAT)
    return int(header)


start_server()
