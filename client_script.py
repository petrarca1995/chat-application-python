import socket
import threading

"""
Stories:
- Right now, program still runs after we type {quit} I need program to stop execution so user can not enter more input, I tried exit(), but does not work
"""

HEADER_LENGTH = 64
PORT = 7023

SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "{quit}"


def add_header_to_message(msg):
    """finds length of message to be sent, then addings space padding to the numeric value and appends actual message to the end"""
    return f'{len(msg):<{HEADER_LENGTH}}' + msg


def send(msg):
    msg_with_header = add_header_to_message(msg)
    client_socket.sendall(bytes(msg_with_header.encode(FORMAT)))


def receive_broadcast_from_server(client_socket):
    """ function executing in its own thread which waits for server to broadcast messages sent by other clients"""
    while True:
        header = client_socket.recv(HEADER_LENGTH).decode(FORMAT)
        # check added because after client sends {quit} flag, server closes the corresponding end, which would
        # trigger an empty byte array being received and was causing an exception
        if not header:
            print("Thanks for chatting with us!")
            # does client also need to close after server closed connection?
            client_socket.close()
            exit()
        msg_length = int(header)
        msg = client_socket.recv(msg_length).decode(FORMAT)
        print(msg)


# create a TCP socket
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
    client_socket.connect(ADDR)
    thread = threading.Thread(target=receive_broadcast_from_server, args=(client_socket,)).start()
    while True:
        msg = input()
        send(msg)
